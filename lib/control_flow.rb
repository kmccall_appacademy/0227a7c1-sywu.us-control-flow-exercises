# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
alphabet = ("a".."z").to_a.join
str.delete(alphabet)
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  if str.length.odd?
    return str[str.length/2]
  else
    return str[(str.length/2)-1..str.length/2]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  vowels = "aeiouAEIOU"
  str.count(vowels)

end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).reduce(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  string = ""
  arr.each_with_index do |char,index|
    string += char
    string += separator if index < arr.length-1
  end
  string
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  str.chars.each_with_index do |char,index|
    if index.odd?
      str[index] = str[index].upcase
    else
      str[index] = str[index].downcase
    end
  end
  str
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  array = []
  str.split.each do |word|
    if word.length > 4
      array << word.reverse
    else
      array << word
    end
  end
  array.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  array = []
  (1..n).each do |int|
    if int % 15 == 0
      array << "fizzbuzz"
    elsif int % 5 == 0
      array << "buzz"
    elsif int % 3 == 0
      array << "fizz"
    else
      array << int
    end
  end
  array
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  new_arr = arr.reverse
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num == 1
  (2..num/2).each do |int|
    return false if num % int == 0
  end
  return true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  array = []
  (1..num).each do |int|
    array << int if num % int == 0
  end
  array
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  array = []
  factors(num).each do |int|
    array << int if prime?(int)
  end
  array
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).count
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odd = []
  even = []
  arr.each do |int|
    if int.odd?
      odd << int
    else
      even << int
    end
  end
  return odd[0] if odd.length < even.length
  return even[0]
end
